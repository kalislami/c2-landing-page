@extends('_layouts.master', ['assets_path' => '../', 'menu' => 'About Us'])

@section('content')
    <div class="container-fluid">

        <div class="row justify-content-center" id="section-au1">
            <div class="container">
                <div class="row">

                    <div class="col-lg-3 mt-5 py-lg-5 pr-lg-5">
                        <div class="py-5 text-center">
                            <h1 class="display-1 mb-0">
                                <ion-icon style="background-color: f7f9fa;" class="p-3 shadow" name="contacts"></ion-icon>
                            </h1>
                            <h1 class="text-shadow font-weight-bold">
                                About Us
                            </h1>
                        </div>
                    </div>
                    <div class="col-lg-9 mt-lg-5 pt-lg-5 pb-lg-0 pb-5 text-center text-lg-left">
                        <h2 class="text-shadow mt-lg-5 font-weight-bold">
                            We believe that
                            <span class="text-success">
                                having a good company culture is not just a bonus, but rather the key for survival
                            </span>
                        </h2>
                        <h2 class="text-shadow font-weight-bold">
                            Our culture is the engine that allows our team to move forward and the principles which we hold on
                            to the tightest
                        </h2>
                    </div>

                </div>
            </div>
        </div>

        <div class="row" id="section-au2">
            <div class="container">
                <h1 class="text-center mt-5 mb-0 text-shadow">
                    Meet The Team
                </h1>
                <hr class="mb-5 mt-0 text-dark bg-dark" style="height: 3px;width: 350px;">
                <div class="row text-center">
                    <div class="col-xl-4 col-md-6 p-lg-5 p-2">
                        <div class="p-3 shadow">
                            <img class="rounded-circle img-fluid" src="http://www.contentcollision.co/assets/team/You.png">
                            <h3>Name</h3>
                            <h4>Position</h4>
                        </div>
                    </div>

                    <div class="col-xl-4 col-md-6 p-lg-5 p-2">
                        <div class="p-3 shadow">
                            <img class="rounded-circle img-fluid" src="http://www.contentcollision.co/assets/team/You.png">
                            <h3>Name</h3>
                            <h4>Position</h4>
                        </div>
                    </div>

                    <div class="col-xl-4 col-md-6 p-lg-5 p-2">
                        <div class="p-3 shadow">
                            <img class="rounded-circle img-fluid" src="http://www.contentcollision.co/assets/team/You.png">
                            <h3>Name</h3>
                            <h4>Position</h4>
                        </div>
                    </div>

                    <div class="col-xl-4 col-md-6 p-lg-5 p-2">
                        <div class="p-3 shadow">
                            <img class="rounded-circle img-fluid" src="http://www.contentcollision.co/assets/team/You.png">
                            <h3>Name</h3>
                            <h4>Position</h4>
                        </div>
                    </div>

                    <div class="col-xl-4 col-md-6 p-lg-5 p-2">
                        <div class="p-3 shadow">
                            <img class="rounded-circle img-fluid" src="http://www.contentcollision.co/assets/team/You.png">
                            <h3>Name</h3>
                            <h4>Position</h4>
                        </div>
                    </div>

                    <div class="col-xl-4 col-md-6 p-lg-5 p-2">
                        <div class="p-3 shadow">
                            <img class="rounded-circle img-fluid" src="http://www.contentcollision.co/assets/team/You.png">
                            <h3>Name</h3>
                            <h4>Position</h4>
                        </div>
                    </div>

                    <div class="col-xl-4 col-md-6 p-lg-5 p-2">
                        <div class="p-3 shadow">
                            <img class="rounded-circle img-fluid" src="http://www.contentcollision.co/assets/team/You.png">
                            <h3>Name</h3>
                            <h4>Position</h4>
                        </div>
                    </div>

                    <div class="col-xl-4 col-md-6 p-lg-5 p-2">
                        <div class="p-3 shadow">
                            <img class="rounded-circle img-fluid" src="http://www.contentcollision.co/assets/team/You.png">
                            <h3>Name</h3>
                            <h4>Position</h4>
                        </div>
                    </div>

                    <div class="col-xl-4 col-md-6 p-lg-5 p-2">
                        <div class="p-3 shadow">
                            <img class="rounded-circle img-fluid" src="http://www.contentcollision.co/assets/team/You.png">
                            <h3>Name</h3>
                            <h4>Position</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row" id="section-au3">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 py-lg-5 py-2">
                        <h2 class="text-center text-lg-left">Team Behavior</h2>
                        <h3 class="text-center text-lg-left">
                            We have fun while thriving on honest feedback and new ideas.
                            Everyone has a voice at Content Collision, and we firmly believe in transparency.
                        </h3>
                    </div>
                    <div class="col-md-6 py-lg-5 py-2">
                        <h5 class="ml-lg-3">We have a couple key policies:</h5>
                        <ul class="team-list">
                            <li>“You ask, we answer.”</li>
                            <li>“Whatever it takes…solve the problem.”</li>
                            <li>“If you don’t get an answer within 24 hours, your idea is approved.”</li>
                            <li>“Move fast, take risks…reflect, analyze…..repeat.”</li>
                            <li>“Question everything, and do it openly.”</li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>

        {{--<div class="row" id="section-au4">--}}
            {{--<div class="container">--}}
                {{--<h1 class="text-center mt-5 text-shadow">--}}
                    {{--Meet The Team--}}
                {{--</h1>--}}
                {{--<div class="row text-center">--}}
                    {{--<div class="col-xl-4 col-md-6 p-lg-5 p-2">--}}
                        {{--<div class="p-3 shadow">--}}
                            {{--<img class="rounded-circle img-fluid" src="http://www.contentcollision.co/assets/team/You.png">--}}
                            {{--<h3>Name</h3>--}}
                            {{--<h4>Position</h4>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    {{--<div class="col-xl-4 col-md-6 p-lg-5 p-2">--}}
                        {{--<div class="p-3 shadow">--}}
                            {{--<img class="rounded-circle img-fluid" src="http://www.contentcollision.co/assets/team/You.png">--}}
                            {{--<h3>Name</h3>--}}
                            {{--<h4>Position</h4>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    {{--<div class="col-xl-4 col-md-6 p-lg-5 p-2">--}}
                        {{--<div class="p-3 shadow">--}}
                            {{--<img class="rounded-circle img-fluid" src="http://www.contentcollision.co/assets/team/You.png">--}}
                            {{--<h3>Name</h3>--}}
                            {{--<h4>Position</h4>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    {{--<div class="col-xl-4 col-md-6 p-lg-5 p-2">--}}
                        {{--<div class="p-3 shadow">--}}
                            {{--<img class="rounded-circle img-fluid" src="http://www.contentcollision.co/assets/team/You.png">--}}
                            {{--<h3>Name</h3>--}}
                            {{--<h4>Position</h4>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    {{--<div class="col-xl-4 col-md-6 p-lg-5 p-2">--}}
                        {{--<div class="p-3 shadow">--}}
                            {{--<img class="rounded-circle img-fluid" src="http://www.contentcollision.co/assets/team/You.png">--}}
                            {{--<h3>Name</h3>--}}
                            {{--<h4>Position</h4>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    {{--<div class="col-xl-4 col-md-6 p-lg-5 p-2">--}}
                        {{--<div class="p-3 shadow">--}}
                            {{--<img class="rounded-circle img-fluid" src="http://www.contentcollision.co/assets/team/You.png">--}}
                            {{--<h3>Name</h3>--}}
                            {{--<h4>Position</h4>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    {{--<div class="col-xl-4 col-md-6 p-lg-5 p-2">--}}
                        {{--<div class="p-3 shadow">--}}
                            {{--<img class="rounded-circle img-fluid" src="http://www.contentcollision.co/assets/team/You.png">--}}
                            {{--<h3>Name</h3>--}}
                            {{--<h4>Position</h4>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    {{--<div class="col-xl-4 col-md-6 p-lg-5 p-2">--}}
                        {{--<div class="p-3 shadow">--}}
                            {{--<img class="rounded-circle img-fluid" src="http://www.contentcollision.co/assets/team/You.png">--}}
                            {{--<h3>Name</h3>--}}
                            {{--<h4>Position</h4>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    {{--<div class="col-xl-4 col-md-6 p-lg-5 p-2">--}}
                        {{--<div class="p-3 shadow">--}}
                            {{--<img class="rounded-circle img-fluid" src="http://www.contentcollision.co/assets/team/You.png">--}}
                            {{--<h3>Name</h3>--}}
                            {{--<h4>Position</h4>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}

        <div class="row text-center" id="section-au5">
            <div class="container">

                <h1 class="text-shadow text-center mb-0">Our Values</h1>
                <hr class="mb-5 mt-0 text-dark bg-dark" style="height: 3px;width: 300px;">

                <p class="font-weight-bold text-center">
                    We believe that having a good company culture is not just a bonus, but rather the key for survival.
                    Our culture is the engine that allows our team to move forward and the principles which we hold on to the tightest.
                    <br><br>
                    Here are some values that Content Collision aims to uphold at all times:
                </p>

                <div class="row justify-content-center">

                    <div class="col-lg-8 col-md-10">
                        <div id="accordion" class="text-md-justify text-left mt-3 shadow">

                            <div class="card border-0">
                                <div class="card-header border-0" id="headingOne">
                                    <h5 class="mb-0">
                                        <a class="collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            1. Be honest and transparent
                                        </a>
                                    </h5>
                                </div>

                                <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="card-body font-weight-bold">
                                        We believe that great relationships are built on trust. Honesty doesn’t mean only
                                        saying positive things, it also often means criticizing someone (and ourselves),
                                        so long as it’s done without malice, and with the intention of helping someone improve.
                                    </div>
                                </div>
                            </div>

                            <div class="card border-0">
                                <div class="card-header border-0">
                                    <h5 class="mb-0">
                                        <a class="collapsed" data-toggle="collapse" data-target="#collapse2" aria-expanded="false" aria-controls="collapseOne">
                                            2. Be independent and proactive
                                        </a>
                                    </h5>
                                </div>

                                <div id="collapse2" class="collapse" data-parent="#accordion">
                                    <div class="card-body font-weight-bold">
                                        We encourage everyone to step up and help solve problems. Along the way,
                                        there will even be problems that the management doesn’t realize exist. Please tell us about it and let’s solve it together.
                                    </div>
                                </div>
                            </div>

                            <div class="card border-0">
                                <div class="card-header border-0">
                                    <h5 class="mb-0">
                                        <a class="collapsed" data-toggle="collapse" data-target="#collapse3" aria-expanded="false" aria-controls="collapseOne">
                                            3. Be a good team player!
                                        </a>
                                    </h5>
                                </div>

                                <div id="collapse3" class="collapse" data-parent="#accordion">
                                    <div class="card-body font-weight-bold">
                                        Good team players don’t just look out for themselves, they help steer the ship.
                                        They are also willing to give feedback to their teammates, as well as
                                        listen to constructive criticism without judgement. The only way for us all
                                        to grow faster is when we can take in as many learnings as possible.
                                    </div>
                                </div>
                            </div>

                            <div class="card border-0">
                                <div class="card-header border-0">
                                    <h5 class="mb-0">
                                        <a class="collapsed" data-toggle="collapse" data-target="#collapse4" aria-expanded="false" aria-controls="collapseOne">
                                            4. Be data oriented
                                        </a>
                                    </h5>
                                </div>

                                <div id="collapse4" class="collapse" data-parent="#accordion">
                                    <div class="card-body font-weight-bold">
                                        What happens if there are two good options in front of you?
                                        Which one do you choose and why? For us, a good answer would be to try both options,
                                        and see which one works best. It’s okay to experiment and let the data do the talking.
                                    </div>
                                </div>
                            </div>

                            <div class="card border-0">
                                <div class="card-header border-0">
                                    <h5 class="mb-0">
                                        <a class="collapsed" data-toggle="collapse" data-target="#collapse5" aria-expanded="false" aria-controls="collapseOne">
                                            5. If you’re unsure, ask!
                                        </a>
                                    </h5>
                                </div>

                                <div id="collapse5" class="collapse" data-parent="#accordion">
                                    <div class="card-body font-weight-bold">
                                        It doesn’t matter who you are, there will always be times when you’re unsure about something.
                                        If you’re unsure, please feel free to ask. Actually, you have the right to ask!
                                    </div>
                                </div>
                            </div>

                            <div class="card border-0">
                                <div class="card-header border-0">
                                    <h5 class="mb-0">
                                        <a class="collapsed" data-toggle="collapse" data-target="#collapse6" aria-expanded="false" aria-controls="collapseOne">
                                            6. Act fast, apologize later
                                        </a>
                                    </h5>
                                </div>

                                <div id="collapse6" class="collapse" data-parent="#accordion">
                                    <div class="card-body font-weight-bold">
                                        You have the autonomy to make the decision which you think is best for everyone.
                                        It’s okay if it’s wrong. We will learn from mistakes. The more mistakes we make,
                                        the more we learn. Just be sure you don’t have to repeatedly learn from the same mistake.
                                    </div>
                                </div>
                            </div>

                            <div class="card border-0">
                                <div class="card-header border-0">
                                    <h5 class="mb-0">
                                        <a class="collapsed" data-toggle="collapse" data-target="#collapse7" aria-expanded="false" aria-controls="collapseOne">
                                            7. Be humble
                                        </a>
                                    </h5>
                                </div>

                                <div id="collapse7" class="collapse" data-parent="#accordion">
                                    <div class="card-body font-weight-bold">
                                        It’s important to be open to learning from anybody. Keep the learning mentality open,
                                        listen to people, and grow as individual.
                                    </div>
                                </div>
                            </div>

                            <div class="card border-0">
                                <div class="card-header border-0">
                                    <h5 class="mb-0">
                                        <a class="collapsed" data-toggle="collapse" data-target="#collapse8" aria-expanded="false" aria-controls="collapseOne">
                                            8. Let’s keep our promises
                                        </a>
                                    </h5>
                                </div>

                                <div id="collapse8" class="collapse" data-parent="#accordion">
                                    <div class="card-body font-weight-bold">
                                        At C2 it doesn’t matter how small, a promise is a promise,
                                        and we should all be able to stand by them. We should honor our promises and keep them.
                                        If we can’t, we should be transparent and honest about why it happened.
                                    </div>
                                </div>
                            </div>

                            <div class="card border-0">
                                <div class="card-header border-0">
                                    <h5 class="mb-0">
                                        <a class="collapsed" data-toggle="collapse" data-target="#collapse9" aria-expanded="false" aria-controls="collapseOne">
                                            9. “Whatever it takes…”
                                        </a>
                                    </h5>
                                </div>

                                <div id="collapse9" class="collapse" data-parent="#accordion">
                                    <div class="card-body font-weight-bold">
                                        This is our motto. It means the minute you mark your name next to an assignment or project,
                                        that task becomes your responsibility and you need to do “whatever it takes”
                                        to get the job done with the highest quality standards you can conceive of.
                                        When you’ve done whatever it takes, there can be no excuses.
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="row pb-5" id="section-contact" style="margin-top: -300px;padding-top: 300px;">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="text-center mt-5 mb-0 text-shadow">Get In Touch</h1>
                        <hr class="mb-5 mt-0 text-dark bg-dark" style="height: 3px;width: 300px;">
                        <p class="font-weight-bold">
                            If you are interested in working with us, or just want to talk about your projects, your goal,
                            or about anything with some talented people, feel free to contact us!
                            <br><br>
                            Fill out the form below and one of our team members will get back to you right away.
                        </p>
                    </div>

                    <div class="col-xl-7 p-3">
                        <form class="shadow p-3" role="form" method="post"action="">

                            <div class="form-group row">
                                <label for="" class="col-sm-3 col-form-label font-italic">Your Name</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control-plaintext border-bottom" name="name">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="" class="col-sm-3 col-form-label font-italic">Email</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control-plaintext border-bottom" name="email">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="" class="col-sm-3 col-form-label font-italic">Subject</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control-plaintext border-bottom" name="subject">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="" class="col-sm-3 col-form-label font-italic">Message</label>
                                <div class="col-sm-9">
                                    <textarea class="form-control-plaintext border-bottom" name="message"> </textarea>
                                </div>
                            </div>

                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-outline-dark mt-3">SEND MESSAGE
                                </button>
                            </div>

                        </form>
                    </div>

                    <div class="col-xl-5 offset-xl-0 col-md-8 offset-md-2 p-3">
                        <div class="shadow p-3 p-lg-5">
                            <div class="row">
                                <div class="col-1">
                                    <ion-icon name="pin"></ion-icon>
                                </div>
                                <div class="col-11">
                                <span>
                                    APL Office Tower, Lantai 16 Unit 9, <br>
                                    Podomoro City (Central Park), <br>
                                    Jl. Let. Jend. S. Parman, Kav. 28 <br>
                                    Jakarta 11470 - Indonesia
                                </span>
                                </div>
                            </div>

                            <div class="row mt-3">
                                <div class="col-1">
                                    <ion-icon name="mail"></ion-icon>
                                </div>
                                <div class="col-11">
                                <span>
                                    <a class="text-secondary" href="mailto:info@contentcollision.co">
                                        info@contentcollision.co
                                    </a>
                                </span>
                                </div>
                            </div>

                            <div class="row mt-3">
                                <div class="col-1">
                                    <ion-icon name="call"></ion-icon>
                                </div>
                                <div class="col-11">
                                <span>
                                    <a>
                                        +62 899 8313 299
                                    </a>
                                </span>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>

        <div id="section-footer" class="row justify-content-center p-md-2 p-1 shadow">
            <p class="mt-3 ml-3 text-center">© 2019 {{ $page->appName }}. All Rights Reserved.</p>
        </div>

    </div>
@endsection