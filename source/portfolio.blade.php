@extends('_layouts.master', ['assets_path' => '../', 'menu' => 'Portfolio'])

@section('content')
    <div class="container-fluid">

        <div class="row justify-content-center" id="section-p1">
            <div class="container">
                <div class="row">

                    <div class="col-lg-3 mt-5 py-lg-5 pr-lg-5">
                        <div class="py-5 text-center">
                            <h1 class="display-1 mb-0">
                                <ion-icon style="background-color: f7f9fa;" class="p-3 shadow" name="copy"></ion-icon>
                            </h1>
                            <h1 class="text-shadow font-weight-bold">
                                Porfolio
                            </h1>
                        </div>
                    </div>
                    <div class="col-lg-9 mt-lg-5 pt-lg-5 pb-lg-0 pb-5 text-center text-lg-left">
                        <h2 class="text-shadow mt-lg-5 font-weight-bold">
                            Our clients
                            <span class="text-success">
                                understand clearly that exceptional content helps break through the noise
                                and attract high quality leads
                            </span>
                        </h2>
                        <h2 class="text-shadow font-weight-bold">
                            Thank you for trusting us as your content partner
                        </h2>
                    </div>

                </div>
            </div>
        </div>

        <div class="row" id="section-p2">
            <div class="container">
                <h1 class="text-center mb-0 text-shadow">
                    Case Studies
                </h1>
                <hr class="mb-5 mt-0 text-light bg-light" style="height: 3px;width: 300px;">
                <div class="row text-center">

                    <div class="col-xl-4 col-lg-6 col-md-6">
                        <a class="case-item" data-toggle="modal" data-target="#modal1">
                            <img src="../assets/images/emeron-thumb.png">
                            <div class="case-bg-cover"></div>
                            <div class="case-logo-cover">
                                <img src="https://s3-ap-southeast-1.amazonaws.com/prod-competition/microsite-asset/competition-emeron/logo-emeron.png">
                            </div>
                            <div class="case-type">Blog</div>
                            <div class="case-desc text-center">
                                Emeron held a blog contest to populate the “Emeron Complete Haircare” keyword on Google
                                with positive content from bloggers. Today, if we...
                            </div>
                        </a>
                    </div>

                    <div class="col-xl-4 col-lg-6 col-md-6">
                        <a class="case-item" data-toggle="modal" data-target="#modal2">
                            <img src="../assets/images/zomato-thumb.png">
                            <div class="case-bg-cover"></div>
                            <div class="case-logo-cover">
                                <img src="../assets/images/zomato.png">
                            </div>
                            <div class="case-type">Press Release</div>
                            <div class="case-desc">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua...
                            </div>
                        </a>
                    </div>

                    <div class="col-xl-4 col-lg-6 col-md-6">
                        <a class="case-item" data-toggle="modal" data-target="#modal3">
                            <img src="../assets/images/thomson-thumb.png">
                            <div class="case-bg-cover"></div>
                            <div class="case-logo-cover">
                                <img src="../assets/images/reuters.png">
                            </div>
                            <div class="case-type">Journalism</div>
                            <div class="case-desc">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua...
                            </div>
                        </a>
                    </div>

                    <div class="col-xl-4 col-lg-6 col-md-6">
                        <a class="case-item" data-toggle="modal" data-target="#modal4">
                            <img src="../assets/images/blibli-thumb.png">
                            <div class="case-bg-cover"></div>
                            <div class="case-logo-cover">
                                <img src="../assets/images/blibli.png">
                            </div>
                            <div class="case-type">Blog</div>
                            <div class="case-desc">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua...
                            </div>
                        </a>
                    </div>

                    <div class="col-xl-4 col-lg-6 col-md-6">
                        <a class="case-item" data-toggle="modal" data-target="#modal5">
                            <img src="../assets/images/gather-thumb.png">
                            <div class="case-bg-cover"></div>
                            <div class="case-logo-cover">
                                <span>Gather</span>
                            </div>
                            <div class="case-type">Press Release</div>
                            <div class="case-desc">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua...
                            </div>
                        </a>
                    </div>

                    <div class="col-xl-4 col-lg-6 col-md-6">
                        <a class="case-item" data-toggle="modal" data-target="#modal6">
                            <img src="../assets/images/indoexpat-thumb.png">
                            <div class="case-bg-cover"></div>
                            <div class="case-logo-cover">
                                <img src="../assets/images/indonesia-expat.png">
                            </div>
                            <div class="case-type">Journalism</div>
                            <div class="case-desc">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua...
                            </div>
                        </a>
                    </div>

                </div>
            </div>
        </div>

        <div class="row" id="section-p3">
            <div class="container text-center">
                <div class="row">
                    <div class="col-md-6">
                        <h1 class="text-shadow">Find our other works:</h1>
                    </div>
                    <div class="col-md-6">
                        <h1 class="text-shadow">
                            <strong style="font-family: 'Lato', sans-serif;">C2 <span style="color: #4a90e2;margin-left: -10px;">live</span></strong>
                            <a class="btn btn-primary btn-lg ml-md-3" style="background-color: #4a90e2;" href="https://contentcollision.c2live.com/"
                               target="_blank">
                                HERE
                            </a>
                        </h1>
                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-center" id="section-p4">
            <div class="container">
                <h2 class="text-center mt-5 mb-0 text-shadow">
                    Our Beloved Clients
                </h2>
                <hr class="mb-5 mt-0 text-dark bg-dark" style="height: 3px;width: 350px;">

                <div id="demo" class="carousel slide" data-ride="carousel">

                    <!-- Indicators -->
                    <ul class="carousel-indicators">
                        <li data-target="#demo" data-slide-to="0" class="active"></li>
                        <li data-target="#demo" data-slide-to="1"></li>
                        <li data-target="#demo" data-slide-to="2"></li>
                        <li data-target="#demo" data-slide-to="3"></li>
                        <li data-target="#demo" data-slide-to="4"></li>
                    </ul>

                    <!-- The slideshow -->
                    <div class="carousel-inner font-weight-bold">
                        <div class="carousel-item active">
                            <div class="row mb-5">
                                <div class="col-md-3 col-6 p-5">
                                    <img class="img-fluid" src="../assets/images/clients/asus.png">
                                </div>
                                <div class="col-md-3 col-6 p-5">
                                    <img class="img-fluid" src="../assets/images/clients/tokopedia.png">
                                </div>
                                <div class="col-md-3 col-6 p-5">
                                    <img class="img-fluid" src="../assets/images/clients/blibli.png">
                                </div>
                                <div class="col-md-3 col-6 p-5">
                                    <img class="img-fluid" src="../assets/images/clients/lazada.png">
                                </div>
                            </div>
                        </div>

                        <div class="carousel-item">
                            <div class="row mb-5">
                                <div class="col-md-3 col-6 p-5">
                                    <img class="img-fluid" src="../assets/images/clients/east-ventures.png">
                                </div>
                                <div class="col-md-3 col-6 p-5">
                                    <img class="img-fluid" src="../assets/images/clients/evercoss.png">
                                </div>
                                <div class="col-md-3 col-6 p-5">
                                    <img class="img-fluid" src="../assets/images/clients/zomato.png">
                                </div>
                                <div class="col-md-3 col-6 p-5">
                                    <img class="img-fluid" src="../assets/images/clients/mustika-ratu.png">
                                </div>
                            </div>
                        </div>

                        <div class="carousel-item">
                            <div class="row mb-5">
                                <div class="col-md-3 col-6 p-5">
                                    <img class="img-fluid" src="../assets/images/clients/telin.png">
                                </div>
                                <div class="col-md-3 col-6 p-5">
                                    <img class="img-fluid" src="../assets/images/clients/mandiri-capital.png">
                                </div>
                                <div class="col-md-3 col-6 p-5">
                                    <img class="img-fluid" src="../assets/images/clients/bca.png">
                                </div>
                                <div class="col-md-3 col-6 p-5">
                                    <img class="img-fluid" src="../assets/images/clients/wings.png">
                                </div>
                            </div>
                        </div>

                        <div class="carousel-item">
                            <div class="row mb-5">
                                <div class="col-md-3 col-6 p-5">
                                    <img class="img-fluid" src="../assets/images/clients/line.png">
                                </div>
                                <div class="col-md-3 col-6 p-5">
                                    <img class="img-fluid" src="../assets/images/clients/emeron.png">
                                </div>
                                <div class="col-md-3 col-6 p-5">
                                    <img class="img-fluid" src="../assets/images/clients/thomson-reuters.png">
                                </div>
                                <div class="col-md-3 col-6 p-5">
                                    <img class="img-fluid" src="../assets/images/clients/guardian.png">
                                </div>
                            </div>
                        </div>

                        <div class="carousel-item">
                            <div class="row mb-5">
                                <div class="col-md-3 col-6 p-5">
                                    <img class="img-fluid" src="../assets/images/clients/scmp.png">
                                </div>
                                <div class="col-md-3 col-6 p-5">
                                    <img class="img-fluid" src="../assets/images/clients/prasmul.png">
                                </div>
                                <div class="col-md-3 col-6 p-5">
                                    <img class="img-fluid" src="../assets/images/clients/skyscanner.png">
                                </div>
                                <div class="col-md-3 col-6 p-5">
                                    <img class="img-fluid" src="../assets/images/clients/cnn.png">
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Left and right controls -->
                    <a class="carousel-control-prev" href="#demo" data-slide="prev">
                        <span class="carousel-control-prev-icon"></span>
                    </a>
                    <a class="carousel-control-next" href="#demo" data-slide="next">
                        <span class="carousel-control-next-icon"></span>
                    </a>
                </div>

                {{--<div class="table-responsive" id="clientList">--}}
                    {{--<div style="display: flex;">--}}
                        {{--<div class="col-lg-3 col-6 p-5">--}}
                            {{--<img class="img-fluid" src="../assets/images/clients/asus.png">--}}
                        {{--</div>--}}
                        {{--<div class="col-lg-3 col-6 p-5">--}}
                            {{--<img class="img-fluid" src="../assets/images/clients/tokopedia.png">--}}
                        {{--</div>--}}
                        {{--<div class="col-lg-3 col-6 p-5">--}}
                            {{--<img class="img-fluid" src="../assets/images/clients/blibli.png">--}}
                        {{--</div>--}}
                        {{--<div class="col-lg-3 col-6 p-5">--}}
                            {{--<img class="img-fluid" src="../assets/images/clients/lazada.png">--}}
                        {{--</div>--}}


                        {{--<div class="col-lg-3 col-6 p-5">--}}
                            {{--<img class="img-fluid" src="../assets/images/clients/east-ventures.png">--}}
                        {{--</div>--}}
                        {{--<div class="col-lg-3 col-6 p-5">--}}
                            {{--<img class="img-fluid" src="../assets/images/clients/evercoss.png">--}}
                        {{--</div>--}}
                        {{--<div class="col-lg-3 col-6 p-5">--}}
                            {{--<img class="img-fluid" src="../assets/images/clients/zomato.png">--}}
                        {{--</div>--}}
                        {{--<div class="col-lg-3 col-6 p-5">--}}
                            {{--<img class="img-fluid" src="../assets/images/clients/mustika-ratu.png">--}}
                        {{--</div>--}}

                        {{--<div class="col-lg-3 col-6 p-5">--}}
                            {{--<img class="img-fluid" src="../assets/images/clients/telin.png">--}}
                        {{--</div>--}}
                        {{--<div class="col-lg-3 col-6 p-5">--}}
                            {{--<img class="img-fluid" src="../assets/images/clients/mandiri-capital.png">--}}
                        {{--</div>--}}
                        {{--<div class="col-lg-3 col-6 p-5">--}}
                            {{--<img class="img-fluid" src="../assets/images/clients/bca.png">--}}
                        {{--</div>--}}
                        {{--<div class="col-lg-3 col-6 p-5">--}}
                            {{--<img class="img-fluid" src="../assets/images/clients/wings.png">--}}
                        {{--</div>--}}

                        {{--<div class="col-lg-3 col-6 p-5">--}}
                            {{--<img class="img-fluid" src="../assets/images/clients/line.png">--}}
                        {{--</div>--}}
                        {{--<div class="col-lg-3 col-6 p-5">--}}
                            {{--<img class="img-fluid" src="../assets/images/clients/emeron.png">--}}
                        {{--</div>--}}
                        {{--<div class="col-lg-3 col-6 p-5">--}}
                            {{--<img class="img-fluid" src="../assets/images/clients/thomson-reuters.png">--}}
                        {{--</div>--}}
                        {{--<div class="col-lg-3 col-6 p-5">--}}
                            {{--<img class="img-fluid" src="../assets/images/clients/guardian.png">--}}
                        {{--</div>--}}

                        {{--<div class="col-lg-3 col-6 p-5">--}}
                            {{--<img class="img-fluid" src="../assets/images/clients/scmp.png">--}}
                        {{--</div>--}}
                        {{--<div class="col-lg-3 col-6 p-5">--}}
                            {{--<img class="img-fluid" src="../assets/images/clients/prasmul.png">--}}
                        {{--</div>--}}
                        {{--<div class="col-lg-3 col-6 p-5">--}}
                            {{--<img class="img-fluid" src="../assets/images/clients/skyscanner.png">--}}
                        {{--</div>--}}
                        {{--<div class="col-lg-3 col-6 p-5">--}}
                            {{--<img class="img-fluid" src="../assets/images/clients/cnn.png">--}}
                        {{--</div>--}}

                    {{--</div>--}}
                {{--</div>--}}

            </div>
        </div>

        @include('_layouts.footer')

        @include('_layouts.modals')

    </div>
@endsection