// navbar effect after scroll
$(window).scroll(function() {
    if ($(document).scrollTop() > 100) {
        $('nav.navbar').addClass('shadow');
    } else {
        $('nav.navbar').removeClass('shadow');
    }
});

// navbar effect after click toggler (mobile view)
$( ".navbar-toggler" ).click(function() {
    if ($('#navbarSupportedContent').hasClass('show')) {
        if ($(document).scrollTop() < 100){
            $('nav.navbar').removeClass('shadow');
        }else {
            $('nav.navbar').addClass('shadow');
        }
    } else {
        $('nav.navbar').addClass('shadow');
    }
});

$(document).ready(function() {
    $('.modal-hdiw').on('hide.bs.modal', function (e) {
        $("#video-player").attr('src', 'https://www.youtube.com/embed/CaUWG1rOfw4');
    })
});