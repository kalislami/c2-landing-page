<nav class="navbar navbar-expand-lg fixed-top navbar-light">

    <div class="container">
        <a class="navbar-brand" href="/">
            <img src="{{ $assets_path }}assets/images/logo-black.png" width="auto" height="50" alt="">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="text-center collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
            </ul>

            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a href="/" class="nav-link text-dark @if($menu == 'Home') active @endif">Home</a>
                </li>
                {{--<li class="nav-item dropdown">--}}
                    {{--<a class="nav-link dropdown-toggle" href="#" id="menuServices" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
                        {{--Services--}}
                    {{--</a>--}}
                    {{--<div class="dropdown-menu" aria-labelledby="menuServices">--}}
                        {{--<a class="dropdown-item" href="{{ $assets_path }}service-content-creation/">Content Creation</a>--}}
                        {{--<a class="dropdown-item" href="#">Content Distribution</a>--}}
                    {{--</div>--}}
                {{--</li>--}}
                <li class="nav-item">
                    <a href="{{ $assets_path }}portfolio/" class="nav-link text-dark @if($menu == 'Portfolio') active @endif">Portfolio</a>
                </li>
                {{--<li class="nav-item">--}}
                    {{--<a href="{{ $assets_path }}about-us/" class="nav-link text-dark @if($menu == 'About Us') active @endif">About Us</a>--}}
                {{--</li>--}}
                <li class="nav-item">
                    <a href="http://blog.contentcollision.co/" class="nav-link text-dark" target="_blank">Blog</a>
                </li>
                <li class="nav-item">
                    <a href="https://www.c2live.com/" class="nav-link text-dark" target="_blank">C2live</a>
                </li>
            </ul>
        </div>
    </div>

</nav>