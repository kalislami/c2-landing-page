<div id="section-contact" class="row">
    <div class="col-xl-6 offset-xl-3 col-lg-8 offset-lg-2 col-md-10 offset-md-1 my-5">
        <h1 class="text-center mb-0">Interested?</h1>
        <hr class="mt-0 mb-5 text-dark bg-dark" style="height: 3px;width: 250px;">
        <form role="form" method="post"action="">

            <div class="form-group row">
                <label for="" class="col-sm-2 col-form-label text-md-right font-italic">Names</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control-plaintext border-bottom" name="name">
                </div>
            </div>

            <div class="form-group row">
                <label for="" class="col-sm-2 col-form-label text-md-right font-italic">I'm From</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control-plaintext border-bottom" name="address">
                </div>
            </div>

            <div class="form-group row">
                <label for="" class="col-sm-4 col-form-label text-md-right font-italic">Reach me via email</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control-plaintext border-bottom" name="email">
                </div>
            </div>

            <div class="form-group text-center">
                <button type="submit" class="btn btn-lg btn-outline-dark mt-3">I'm Interested
                </button>
            </div>

        </form>
    </div>
</div>

<div id="section-footer" class="row justify-content-center p-md-2 p-1 shadow">
    <p class="mt-3 ml-3 text-center">© 2019 {{ $page->appName }}. All Rights Reserved.</p>
</div>