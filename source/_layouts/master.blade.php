<!DOCTYPE html>
<html lang="en">

@include('_layouts.head')

<body>

@include('_layouts.navbar')

@yield('content')

@include('_layouts.scripts')

</body>

</html>

