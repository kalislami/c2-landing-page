<div class="modal fade animated fadeInLeft" id="modal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content rounded-0 shadow-lg">
            <div class="modal-header">
                <div class="row">
                    <div class="col-3">
                        <img class="img-fluid"
                             src="https://s3-ap-southeast-1.amazonaws.com/prod-competition/microsite-asset/competition-emeron/logo-emeron.png">
                    </div>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <h1 class="display-5 mb-0">
                        <ion-icon name="close-circle"></ion-icon>
                    </h1>
                </button>
            </div>
            <div class="modal-body">
                <h1 class="text-center mb-5 text-shadow">Emeron Complete Haircare</h1>
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <div class="shadow p-4">
                            <h4>Case:</h4>
                            <p>
                                Emeron held a blog contest to populate the “Emeron Complete Haircare” keyword on Google with
                                positive content from bloggers. Today, if we search that keyword, around 4 - 5 of blogger
                                articles will appear on the first page.
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="shadow p-4">
                            <h4>References:</h4>
                            <ol>
                                <li>
                                    <a target="_blank" href="https://competition.c2live.com/perawatan-lengkap-untuk-rambut-cantikmu">
                                        Contest Microsite
                                    </a>
                                </li>

                                <li>
                                    <a target="_blank" href="https://yasinyasintha.com/emeron-complete-hair-care-rambut-halus-lembut/">
                                        Article #1
                                    </a>
                                </li>

                                <li>
                                    <a target="_blank" href="https://www.trianiretno.com/2018/05/emeron-complete-hair-care-lembut-merawat-rambut.html">
                                        Article #2
                                    </a>
                                </li>

                                <li>
                                    <a target="_blank" href="https://www.impiccha.com/2018/06/review-emeron-complete-haircare-soft.html">
                                        Article #3
                                    </a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>

<div class="modal fade animated fadeInUpBig" id="modal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content rounded-0 shadow-lg">
            <div class="modal-header">
                <div class="row">
                    <div class="col-3">
                        <img class="img-fluid"
                             src="../assets/images/zomato.png">
                    </div>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <h1 class="display-5 mb-0">
                        <ion-icon name="close-circle"></ion-icon>
                    </h1>
                </button>
            </div>
            <div class="modal-body">
                <h1 class="text-center mb-5 text-shadow">Zomato Press Release</h1>
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <div class="shadow p-4">
                            <h4>Case:</h4>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure
                                dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                                Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt
                                mollit anim id est laborum.
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="shadow p-4">
                            <h4>References:</h4>
                            <ol>
                                <li>
                                    <a target="_blank" href="#">
                                        Reference #1
                                    </a>
                                </li>

                                <li>
                                    <a target="_blank" href="#">
                                        Reference #2
                                    </a>
                                </li>

                                <li>
                                    <a target="_blank" href="#">
                                        Reference #3
                                    </a>
                                </li>

                                <li>
                                    <a target="_blank" href="#">
                                        Reference #4
                                    </a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>

<div class="modal fade animated fadeInRight" id="modal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content rounded-0 shadow-lg">
            <div class="modal-header">
                <div class="row">
                    <div class="col-3">
                        <img class="img-fluid"
                             src="../assets/images/reuters.png">
                    </div>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <h1 class="display-5 mb-0">
                        <ion-icon name="close-circle"></ion-icon>
                    </h1>
                </button>
            </div>
            <div class="modal-body">
                <h1 class="text-center mb-5 text-shadow">Thomson Reuters Jurnalism</h1>
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <div class="shadow p-4">
                            <h4>Case:</h4>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure
                                dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                                Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt
                                mollit anim id est laborum.
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="shadow p-4">
                            <h4>References:</h4>
                            <ol>
                                <li>
                                    <a target="_blank" href="#">
                                        Reference #1
                                    </a>
                                </li>

                                <li>
                                    <a target="_blank" href="#">
                                        Reference #2
                                    </a>
                                </li>

                                <li>
                                    <a target="_blank" href="#">
                                        Reference #3
                                    </a>
                                </li>

                                <li>
                                    <a target="_blank" href="#">
                                        Reference #4
                                    </a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>

<div class="modal fade animated rotateInDownLeft" id="modal4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content rounded-0 shadow-lg">
            <div class="modal-header">
                <div class="row">
                    <div class="col-3">
                        <img class="img-fluid"
                             src="../assets/images/blibli.png">
                    </div>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <h1 class="display-5 mb-0">
                        <ion-icon name="close-circle"></ion-icon>
                    </h1>
                </button>
            </div>
            <div class="modal-body">
                <h1 class="text-center mb-5 text-shadow">Blibli Blog Competition</h1>
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <div class="shadow p-4">
                            <h4>Case:</h4>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure
                                dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                                Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt
                                mollit anim id est laborum.
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="shadow p-4">
                            <h4>References:</h4>
                            <ol>
                                <li>
                                    <a target="_blank" href="#">
                                        Reference #1
                                    </a>
                                </li>

                                <li>
                                    <a target="_blank" href="#">
                                        Reference #2
                                    </a>
                                </li>

                                <li>
                                    <a target="_blank" href="#">
                                        Reference #3
                                    </a>
                                </li>

                                <li>
                                    <a target="_blank" href="#">
                                        Reference #4
                                    </a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>

<div class="modal fade animated zoomIn" id="modal5" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content rounded-0 shadow-lg">
            <div class="modal-header">
                <div class="row">
                    <div class="col-3">
                        <h1 class="display-5 mb-0">
                            <span>Gather</span>
                        </h1>
                    </div>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <h1 class="display-5 mb-0">
                        <ion-icon name="close-circle"></ion-icon>
                    </h1>
                </button>
            </div>
            <div class="modal-body">
                <h1 class="text-center mb-5 text-shadow">Gather Press Release</h1>
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <div class="shadow p-4">
                            <h4>Case:</h4>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure
                                dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                                Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt
                                mollit anim id est laborum.
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="shadow p-4">
                            <h4>References:</h4>
                            <ol>
                                <li>
                                    <a target="_blank" href="#">
                                        Reference #1
                                    </a>
                                </li>

                                <li>
                                    <a target="_blank" href="#">
                                        Reference #2
                                    </a>
                                </li>

                                <li>
                                    <a target="_blank" href="#">
                                        Reference #3
                                    </a>
                                </li>

                                <li>
                                    <a target="_blank" href="#">
                                        Reference #4
                                    </a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>

<div class="modal fade animated rotateInDownRight" id="modal6" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content rounded-0 shadow-lg">
            <div class="modal-header">
                <div class="row">
                    <div class="col-3">
                        <img class="img-fluid"
                             src="../assets/images/indonesia-expat.png">
                    </div>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <h1 class="display-5 mb-0">
                        <ion-icon name="close-circle"></ion-icon>
                    </h1>
                </button>
            </div>
            <div class="modal-body">
                <h1 class="text-center mb-5 text-shadow">Indonesia Expat Journalism</h1>
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <div class="shadow p-4">
                            <h4>Case:</h4>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure
                                dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                                Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt
                                mollit anim id est laborum.
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="shadow p-4">
                            <h4>References:</h4>
                            <ol>
                                <li>
                                    <a target="_blank" href="#">
                                        Reference #1
                                    </a>
                                </li>

                                <li>
                                    <a target="_blank" href="#">
                                        Reference #2
                                    </a>
                                </li>

                                <li>
                                    <a target="_blank" href="#">
                                        Reference #3
                                    </a>
                                </li>

                                <li>
                                    <a target="_blank" href="#">
                                        Reference #4
                                    </a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>