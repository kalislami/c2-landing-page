<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Page title -->
    <title> {{ $page->appName }} | {{ $menu }} </title>
    <meta name="description" content="{{ $page->appDesc }}"/>

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <link rel="icon" type="image/png" href="{{ $assets_path }}assets/images/favicon.png"/>

    <!-- ionicons -->
    <link href="https://unpkg.com/ionicons@4.5.5/dist/css/ionicons.min.css" rel="stylesheet">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>

    <!-- Twitter Card data -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="@CollisionC2">
    <meta name="twitter:title" content="{{ $page->appName }}">
    <meta name="twitter:description" content="{{ $page->appDesc }}">
    <meta name="twitter:image" content="{{ $page->baseUrl . 'assets/images/logo-black.png' }}">

    <!-- Open Graph data -->
    <meta property="og:title" content="{{ $page->appName }}"/>
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="{{ $page->baseUrl }}"/>
    <meta property="og:image" content="{{ $page->baseUrl . 'assets/images/logo-black.png' }}"/>
    <meta property="og:description" content="{{ $page->appDesc }}"/>

    <!-- Style -->
    <link rel="stylesheet" href="{{ mix('css/main.css', 'assets/build') }}">

    <style>
        .modal-content {
            background-image: linear-gradient(180deg,#f3f3f3, #f7f9fa, #fff);
        }
        .modal-body::before {
             content: "";
             opacity: 0.03;
             position: absolute;
             top: 0;
             left: 0;
             bottom: 0;
             right: 0;
             background-image: url(/assets/images/emeron-thumb.png);
             background-position: 100% -800px;
             background-size: 100%;
             background-repeat: no-repeat;
             box-shadow: inset 0 300px 70px -25px rgba(0, 0, 0, 0.05);
        }
        #section-p4 img, #section-h5 img {
            transition: transform .4s;
        }

        #section-p4 img:hover, #section-h5 img:hover {
            transform: scale(1.1);
        }
    </style>

</head>