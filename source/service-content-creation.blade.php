@extends('_layouts.master', ['assets_path' => '../', 'menu' => 'Services'])

@section('content')
    <div class="container-fluid">

        <div class="row justify-content-center" id="section-h1">
            <div class="container">
                <div class="row">

                    <div class="col-xl-7 mt-5 py-5 pr-lg-5">
                        <div class="py-5 text-center">
                            <h1 class="display-1 mb-0 rotate-md-3">
                                <ion-icon class="rounded-circle border border-dark p-3" name="clipboard"></ion-icon>
                            </h1>
                            <h1 class="rotate-md-3">
                                Content Creation
                            </h1>
                        </div>
                    </div>
                    <div class="col-xl-5 img-header mt-5 pt-5">
                        <img class="img-fluid shadow my-5" src="../assets/images/bg-img-service-intro.png">
                    </div>

                </div>
            </div>
        </div>

        <div class="row" id="section-h2">
            <div class="container">
                <h1 class="text-warning display-1 mb-0 float-right rotate-md3">
                    <ion-icon class="rounded shadow bg-warning p-3 text-white" name="clipboard"></ion-icon>
                </h1>
                <h3 class="text-center text-lg-left">
                    Let the audience <span class="text-warning">experience</span> your brand <span class="text-warning">through relevant and engaging stories</span>.
                </h3>
                <h3 class="text-center text-lg-left">
                    Whether your stories are told via <span class="text-warning">articles, images, or videos</span>, we’ve got your back.
                </h3>
            </div>
            <div class="container mt-5">
                <h1 class="text-center mb-5">Products</h1>
                <div class="row">

                    <div class="col-md-4">
                        <div class="card border-0 shadow">
                            <h1 class="display-1 mb-0 text-center text-warning">
                                <ion-icon class="p-3" name="list-box"></ion-icon>
                            </h1>
                            <div class="card-body">
                                <h5 class="card-title">
                                    <u>POST / ARTICLES</u>
                                </h5>
                                <p class="card-text">
                                    This should be your primary ammunition if you’re looking to <strong>boost organic SEO</strong>
                                    for your website. While attracting traffic using ads is often tempting,
                                    powerful content is your answer to long term and sustainable growth.
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="card border-0 shadow">
                            <h1 class="display-1 mb-0 text-center text-warning">
                                <ion-icon class="p-3" name="paper"></ion-icon>
                            </h1>
                            <div class="card-body">
                                <h5 class="card-title">
                                    <u>IMAGE / INFOGRAPHIC</u>
                                </h5>
                                <p class="card-text">
                                    No brand can ignore social media, as this is the realm where <strong>images and infographics thrive</strong>.
                                    Your audience is hungry for easy-to-digest content, and putting out eye-catching
                                    graphics is a great way to pique their interests.
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="card border-0 shadow">
                            <h1 class="display-1 mb-0 text-center text-warning">
                                <ion-icon class="p-3" name="list-box"></ion-icon>
                            </h1>
                            <div class="card-body">
                                <h5 class="card-title">
                                    <u>POST / ARTICLES</u>
                                </h5>
                                <p class="card-text">
                                    This should be your primary ammunition if you’re looking to <strong>boost organic SEO</strong>
                                    for your website. While attracting traffic using ads is often tempting,
                                    powerful content is your answer to long term and sustainable growth.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @include('_layouts.footer')

    </div>
@endsection