@extends('_layouts.master', ['assets_path' => '', 'menu' => 'Home'])

@section('content')
    <div class="container-fluid">

        <div class="row justify-content-center" id="section-h1">
            <div class="container">
                <div class="row">

                    <div class="col-lg-6 mt-5 py-5 pr-lg-5">
                        <div class="py-lg-5 pt-5">
                            <h1 class="text-shadow font-weight-bold">
                                We <span class="text-success">guarantee ROI for brands and publishers</span>
                            </h1>
                            <h1 class="text-shadow font-weight-bold mt-xl-5">
                                so they can dominate the world with content
                            </h1>
                        </div>
                    </div>
                    <div class="col-lg-6 mt-lg-5 py-lg-5">
                        <div class="pt-lg-5 pl-lg-5">
                            <div class="embed-responsive embed-responsive-16by9 shadow">
                                <iframe id="video-player" class="embed-responsive-item" src="https://www.youtube.com/embed/CaUWG1rOfw4" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 pb-lg-5 py-3 text-center">
                        <a href="#section-contact" class="btn btn-lg btn-success">
                            I’m Interested!
                        </a>
                    </div>

                </div>
            </div>
        </div>

        <div class="row" id="section-h2">
            <div class="container">
                <h1 class="text-center mb-0 text-shadow">About us</h1>
                <hr class="mb-5 mt-0 text-dark bg-dark" style="height: 3px;width: 250px;">
                <p class="text-center text-lg-left font-weight-bold">
                    Content Collision helps brands and publishers dominate the world with content.
                    Through the stories we tell, the public will enjoy learning about your brand.
                    We compose one-of-a-kind, in-depth content campaigns for your brand that can’t be found anywhere else.
                </p>
                <p class="text-center text-lg-left font-weight-bold">
                    We also aim to nurture the freelance writing ecosystem by providing a consistent and reliable flow of
                    work for the world’s most talented writers, allowing them to build and polish their portfolios
                    in the process.
                </p>

            </div>
        </div>

        <div class="row" id="section-h3">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 py-5">
                        <h2 class="text-shadow mb-3 text-center text-md-left">Offerings</h2>
                        <h5 class="text-center text-md-left">
                            There’s no one like us! We offer performance-based content and distribution services for
                            international brands and publishers. You only pay for what you get. Nothing more, nothing less.
                        </h5>
                    </div>
                    <div class="col-md-6">
                        <div class="row mb-3">
                            <div class="col-3 text-right">
                                <h4 class="mb-0">
                                    <ion-icon class="rounded-circle border p-3" name="clipboard"></ion-icon>
                                </h4>
                            </div>
                            <div class="col-9">
                                <h4 class="mt-3">Content Marketing</h4>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <div class="col-3 text-right">
                                <h4 class="mb-0">
                                    <ion-icon class="rounded-circle border p-3" name="paper"></ion-icon>
                                </h4>
                            </div>
                            <div class="col-9">
                                <h4 class="mt-3">Digital PR</h4>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-3 text-right">
                                <h4 class="mb-0">
                                    <ion-icon class="rounded-circle border p-3" name="photos"></ion-icon>
                                </h4>
                            </div>
                            <div class="col-9">
                                <h4 class="mt-3">Content Platform</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-center" id="section-h4">

            <div class="container">
                <h1 class="text-center mb-0 text-shadow">Testimonials</h1>
                <hr class="mt-0 mb-5 text-dark bg-dark" style="height: 3px;width: 300px;">

                <div id="demo" class="carousel slide" data-ride="carousel">

                    <!-- Indicators -->
                    <ul class="carousel-indicators">
                        <li data-target="#demo" data-slide-to="0" class="active"></li>
                        <li data-target="#demo" data-slide-to="1"></li>
                    </ul>

                    <!-- The slideshow -->
                    <div class="carousel-inner font-weight-bold">
                        <div class="carousel-item active">
                            <p class="text-center">
                                "Content Collision is a professionally run media agency with extremely motivated individuals.
                                They helped us, a startup, tremendously with our media outreach, and got the message across
                                about the achievements of our team and brand."
                            </p>
                            <p class="text-center">
                                "We were also particularly impressed by their quick response times and timely follow ups.
                                This was extremely useful along with the good quality content they put out,
                                all with extremely competitive pricing, which went a long way in convincing us that they are
                                the ones to partner with in the long run."
                            </p>
                            <p class="text-center">-Karthik Shetty (Zomato Indonesia Country Manager)</p>
                        </div>
                        <div class="carousel-item">
                            <p class="text-center">
                                "For us, C2 stands for Collision and Collaboration. The team has tons of energy and always looks to be more creative.
                                It’s no wonder they were able to steal our attention and push us to be more creative as well."
                            </p>
                            <p class="text-center">
                                "They are willing to grow alongside us and the focus never seems to be on their own success, but on ours.
                                They are always willing to learn, not just earn.
                                That’s what make C2 different from any other partner out there. We are lucky to work with them."
                            </p>
                            <p class="text-center">-Nicky Sebastian (Sr. Communication Strategist at Blibli.com)</p>
                        </div>
                    </div>

                    <!-- Left and right controls -->
                    <a class="carousel-control-prev" href="#demo" data-slide="prev">
                        <span class="carousel-control-prev-icon"></span>
                    </a>
                    <a class="carousel-control-next" href="#demo" data-slide="next">
                        <span class="carousel-control-next-icon"></span>
                    </a>
                </div>
            </div>

        </div>

        <div class="row justify-content-center p-lg-5 py-5" id="section-h5">
            <div class="container">
                <div class="row">

                    <div class="col-lg-3 col-6 p-5">
                        <img class="img-fluid" src="assets/images/clients/asus.png">
                    </div>
                    <div class="col-lg-3 col-6 p-5">
                        <img class="img-fluid" src="assets/images/clients/tokopedia.png">
                    </div>
                    <div class="col-lg-3 col-6 p-5">
                        <img class="img-fluid" src="assets/images/clients/blibli.png">
                    </div>
                    <div class="col-lg-3 col-6 p-5">
                        <img class="img-fluid" src="assets/images/clients/lazada.png">
                    </div>
                    <div class="col-lg-3 col-6 p-5">
                        <img class="img-fluid" src="assets/images/clients/mustika-ratu.png">
                    </div>
                    <div class="col-lg-3 col-6 p-5">
                        <img class="img-fluid" src="assets/images/clients/emeron.png">
                    </div>
                    <div class="col-lg-3 col-6 p-5">
                        <img class="img-fluid" src="assets/images/clients/wings.png">
                    </div>
                    <div class="col-lg-3 col-6 p-5">
                        <img class="img-fluid" src="assets/images/clients/guardian.png">
                    </div>
                    <div class="col-lg-3 col-6 p-5">
                        <img class="img-fluid" src="assets/images/clients/zomato.png">
                    </div>
                    <div class="col-lg-3 col-6 p-5">
                        <img class="img-fluid" src="assets/images/clients/bca.png">
                    </div>
                    <div class="col-lg-3 col-6 p-5">
                        <img class="img-fluid" src="assets/images/clients/scmp.png">
                    </div>
                    <div class="col-lg-3 col-6 p-5">
                        <img class="img-fluid" src="assets/images/clients/cnn.png">
                    </div>
                    <div class="col-lg-12 text-center mt-5">
                        <a href="/portfolio" class="btn btn-lg btn-outline-dark">Our Portfolio</a>
                    </div>

                </div>
            </div>
        </div>

        @include('_layouts.footer')

        {{--modal how does it work--}}
        <div class="modal fade modal-hdiw" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title text-shadow">How does it work?</h5>
                        <a class="text-danger" href="#" data-dismiss="modal">
                            <h2 class="mb-0">
                                <ion-icon name="close-circle"></ion-icon>
                            </h2>
                        </a>
                    </div>
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe id="video-player" class="embed-responsive-item" src="https://www.youtube.com/embed/CaUWG1rOfw4" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection