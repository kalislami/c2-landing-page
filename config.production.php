<?php

return [
    'appName' => 'Content Collision',
    'appDesc' => 'Content Collision helps brands and publishers dominate the world with content.',
    'production' => true,
    'baseUrl' => 'http://www.contentcollision.co/',
    'collections' => [],
];
